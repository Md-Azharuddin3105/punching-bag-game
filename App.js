new Vue({
    el: '#vue-app',
    data: {
        health: 100,
        ended: false
    },
    methods: {
        punch: function(){
            this.health -= 10;
            if ( this.health <= 0 ){
                this.ended = true;
            }
        },
        restart: function(){
            this.health = 100;
            this.ended = false;
        },
        createStyle: function(){
            let background = 'green'
             let width = this.health + '%'
             if(this.health <= 50 && this.health > 20){
                 background = 'yellow'
             }
             if(this.health <= 20){
                 background = 'crimson'
             }
             console.log({width,background})
             return{
                 width,
                 background
             }
        }
    },
    computed: {

    }
});